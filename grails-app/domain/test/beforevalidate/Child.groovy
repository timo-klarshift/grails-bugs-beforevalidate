package test.beforevalidate

class Child {
    static belongsTo = [parent: Parent]

    String name

    static constraints = {

    }

    def beforeValidate(){
        name = name ?: 'auto-name'
    }
}

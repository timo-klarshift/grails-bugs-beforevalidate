package test.beforevalidate

class Parent {
    static hasMany = [children: Child]

    String name

    static constraints = {
    }

    def beforeValidate(){
        name = name ?: 'auto-name'
    }
}

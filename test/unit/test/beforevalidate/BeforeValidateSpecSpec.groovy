package test.beforevalidate

import grails.test.mixin.TestMixin
import grails.test.mixin.hibernate.HibernateTestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import grails.test.runtime.SharedRuntime
import spock.lang.Specification
import test.beforeValidate.CustomTestRuntimeConfigurer

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */

@TestMixin(HibernateTestMixin)
@SharedRuntime(CustomTestRuntimeConfigurer)
class BeforeValidateSpecSpec extends Specification {
    void setupSpec() {
        hibernateDomain([
                Child,
                Parent,
        ])
    }


    def setup() {
    }

    def cleanup() {
    }

    void "test if beforeValidate() get called on child when validating parent"() {
        when: 'creating associations'
        def parent = new Parent(name: 'mr spock')

        then: 'parent can be saved'
        parent.validate()
        parent.save()
        parent.name == 'mr spock'

        when: 'validating parent with nameless child'
        def child = new Child()
        println child
        parent.addToChildren(child)

        then: 'child name must be set automatically'
        parent.validate()
        child.name != null
        child.validate()
        parent.save()
    }

    void "test if beforeValidate() get called before saving the parent"(){
        when: 'storing a nameless parent'
        def parent = new Parent()
        then: 'save must not fail, because we set the name in beforeValidate()'
        parent.save()
        println parent.name
        parent.name == 'auto-name'
    }



    void "test that it works with manually calling beforeValidate()"() {
        given: " a parent"

        def parent = new Parent(name: 'mr Spock')
        def child = new Child()
        parent.addToChildren(child)

        when: 'calling beforeValidate manually'
        child.beforeValidate()
        then: 'child and parent validate and parent can be saved'
        child.name
        child.validate()
        parent.validate()
        parent.save()

        println "Child name is: ${child.name}"
    }
}

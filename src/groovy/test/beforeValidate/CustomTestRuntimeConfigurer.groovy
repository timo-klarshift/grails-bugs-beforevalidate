package test.beforeValidate

/**
 *
 * User: timo (timo@klarshift.de)
 * Date: 28.07.14
 * Time: 12:45
 */
import grails.test.runtime.*
import org.codehaus.groovy.grails.commons.GrailsApplication
import org.codehaus.groovy.grails.orm.hibernate.cfg.HibernateUtils

class CustomTestRuntimeConfigurer implements SharedRuntimeConfigurer, TestEventInterceptor, TestPluginRegistrar {
    CustomTestRuntimeConfigurer(){
        super()
    }
    @Override
    public String[] getRequiredFeatures() {
        return [] as String[];
    }

    @Override
    public void configure(TestRuntime runtime) {

    }

    @Override
    public void eventPublished(TestEvent event) {

    }

    @Override
    public void eventDelivered(TestEvent event) {
        if(event.name == 'applicationInitialized') {
            enhanceDomains(event.runtime, (GrailsApplication)event.arguments.grailsApplication)
        }
    }

    @Override
    public void eventsProcessed(TestEvent event, List<TestEvent> consequenceEvents) {

    }

    @Override
    public void mutateDeferredEvents(TestEvent event, List<TestEvent> deferredEvents) {

    }

    void enhanceDomains(TestRuntime runtime, GrailsApplication grailsApplication) {
        HibernateUtils.enhanceSessionFactories(grailsApplication.mainContext, grailsApplication)
    }

    @Override
    public Iterable<TestPluginUsage> getTestPluginUsages() {
        TestPluginUsage.createForActivating(CustomGrailsApplicationTestPlugin)
    }
}



package test.beforeValidate

import grails.test.runtime.GrailsApplicationTestPlugin
import grails.test.runtime.TestEvent

/**
 *
 * User: timo (timo@klarshift.de)
 * Date: 28.07.14
 * Time: 13:12
 */
class CustomGrailsApplicationTestPlugin extends GrailsApplicationTestPlugin {
    public CustomGrailsApplicationTestPlugin() {
        super()
        ordinal = 1
    }

    @Override
    public void onTestEvent(TestEvent event) {
        if(event.name != 'after') {
            super.onTestEvent(event);
        } else {
            println "blocked ${event}"
        }
    }
}
